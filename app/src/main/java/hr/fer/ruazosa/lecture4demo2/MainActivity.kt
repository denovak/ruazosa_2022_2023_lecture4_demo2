package hr.fer.ruazosa.lecture4demo2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import hr.fer.ruazosa.lecture4demo2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.addRemoveFragmentButton.setOnClickListener {
            val myFragment = supportFragmentManager.findFragmentByTag("myFragmentTag")

            if (myFragment == null) {
                val transaction = supportFragmentManager.beginTransaction()
                transaction.add(binding.fragmentHolderView.id, HelloWorldFragment(), "myFragmentTag")
                transaction.commit()
            }
            else {
                val transaction = supportFragmentManager.beginTransaction()
                transaction.remove(myFragment)
                transaction.commit()
            }

        }
    }
}